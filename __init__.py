from Robot import Robot

if __name__ == "__main__":
    print("LIMPIA CASAS")

    r = input("\nNombre del Robot: ")
    robot = Robot(r)

    while True:
        if robot.nivel_bateria <= 0:
            print("Me quede sin bateria...")
            break

        print("\n\nMenu - Robot - " + robot.nombre)
        print("1. Barrer")
        print("2. Fregar")
        print("3. Hablar")
        print("4. Ver nivel de bateria")
        print("5. Salir")

        try:
            o = int(input("\nOpcion: "))
        except:
            o = -15
        if o == 1:
            print(robot.barrer())
        elif o == 2:
            print(robot.fregar())
        elif o == 3:
            robot.hablar()
        elif o == 4:
            print(robot.nivel_bateria)
        elif o == 5:
            print("Ciao")
            break
        else:
            print("Opcion invalida")

